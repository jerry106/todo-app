package com.example.todoapp.service;


import com.example.todoapp.entity.ToDoBoard;
import com.example.todoapp.paging.PaginationRequest;
import com.example.todoapp.paging.PaginationResponse;


public interface ToDoService {

    void register(ToDoBoard toDoBoard);

    PaginationResponse search(String keyword, PaginationRequest paginationRequest);

    void modify(ToDoBoard toDoBoard);

    void remove(Long id);

    ToDoBoard read(Long id);

    void finishOrReset(Long id);
}
