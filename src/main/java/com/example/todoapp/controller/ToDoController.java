package com.example.todoapp.controller;

import com.example.todoapp.entity.ToDoBoard;
import com.example.todoapp.paging.PaginationRequest;
import com.example.todoapp.paging.PaginationResponse;
import com.example.todoapp.service.ToDoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/todo")
@RequiredArgsConstructor
public class ToDoController {

    @Autowired
    private ToDoService toDoService;

    @PostMapping("/register")
    public void todoRegister(@Validated @RequestBody ToDoBoard toDoBoard){
        log.info("todoRegister()");

        toDoService.register(toDoBoard);
    }

    @GetMapping("/search")
    public PaginationResponse todoSearch(@RequestParam(defaultValue = "") String keyword, PaginationRequest paginationRequest){
        log.info("todoSearch()");

        return toDoService.search(keyword, paginationRequest);
    }

    @GetMapping("/{id}")
    public ToDoBoard todoRead(@PathVariable Long id){
        log.info("todoRead()");

        return toDoService.read(id);
    }

    @PutMapping("/{id}")
    public void todoModify(@PathVariable Long id, @Validated @RequestBody ToDoBoard toDoBoard){
        log.info("todoModify()");

        toDoBoard.setId(id);

        toDoService.modify(toDoBoard);
    }

    @DeleteMapping("/{id}")
    public void todoRemove(@PathVariable Long id){
        log.info("todoRemove()");

        toDoService.remove(id);
    }

    @PutMapping("/finishOrReset/{id}")
    public void todoFinishOrReset(@PathVariable Long id){
        log.info("todoFinishOrReset()");

        toDoService.finishOrReset(id);
    }
}
