package com.example.todoapp.repository;

import com.example.todoapp.entity.ToDoBoard;
import com.example.todoapp.paging.PaginationRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@Mapper
public interface ToDoRepository {

    @Insert("insert into ToDoBoard(title, content, state, createdDateTime) values(#{title}, #{content}, #{state}, #{createdDateTime})")
    void register(ToDoBoard toDoBoard);

    @Select("select * from ToDoBoard where title iLike '%' || #{keyword} || '%' or content iLike '%' || #{keyword} || '%' order by createdDateTime DESC limit #{paginationRequest.pageSize} offset ${paginationRequest.pageNo * paginationRequest.pageSize}")
    List<ToDoBoard> search(String keyword, PaginationRequest paginationRequest);

    @Update("update ToDoBoard set title = #{title}, content = #{content}, state = #{state}, updatedDateTime = #{updatedDateTime} where id = #{id}")
    void modify(ToDoBoard toDoBoard);

    @Delete("delete from ToDoBoard where id = #{id}")
    void remove(Long id);

    @Select("select * from ToDoBoard where id = #{id}")
    ToDoBoard read(Long id);

    @Select("select count(*) from ToDoBoard where title iLike '%' || #{keyword} || '%' or content iLike '%' || #{keyword} || '%'")
    int searchCount(String keyword);

    @Update({
            "<script>",
            "  UPDATE Author",
            "  <set>",
            "    <if test='USER_NAME != null'> USER_NAME = #{USER_NAME} , </if>",
            "    <if test='USER_AGE != null'> USER_AGE = #{USER_AGE} , </if>",
            "    <if test='USER_MAIL != null'> USER_MAIL = #{USER_MAIL} </if>",
            "  </set>",
            "  WHERE USER_ID = #{USER_ID}",
            "</script>"
    })
    void test();
}
