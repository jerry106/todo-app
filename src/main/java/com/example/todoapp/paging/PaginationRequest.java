package com.example.todoapp.paging;

import lombok.*;

@Data
@AllArgsConstructor
public class PaginationRequest {

    // 페이지 번호
    private int pageNo;

    // 한 페이지 데이터 수
    private int pageSize;

    // 총 페이지 수
    public PaginationRequest() {
        this(0,10);
    }
}
