package com.example.todoapp.paging;

import com.example.todoapp.entity.ToDoBoard;
import lombok.*;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaginationResponse {

    // 페이지 번호
    private int pageNo;

    // 한 페이지 데이터 수
    private int pageSize;

    // 총 페이지 수
    private int total;

    //총 데이터의 수
    private int amount;

    // todo데이터
    List<ToDoBoard> lists;

    public PaginationResponse(int pageNo, int pageSize) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }
}
