package com.example.todoapp.service;

import com.example.todoapp.entity.ToDoBoard;
import com.example.todoapp.paging.PaginationRequest;
import com.example.todoapp.paging.PaginationResponse;
import com.example.todoapp.repository.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class ToDoServiceImpl implements ToDoService{

    @Autowired
    private ToDoRepository toDoRepository;

    @Override
    public void register(ToDoBoard toDoBoard) {

        toDoRepository.register(toDoBoard);
    }

    @Override
    public PaginationResponse search(String keyword, PaginationRequest paginationRequest) {

        int boardCnt = toDoRepository.searchCount(keyword);
        int total;

        PaginationResponse paginationResponse = new PaginationResponse(paginationRequest.getPageNo(), paginationRequest.getPageSize());

        if (boardCnt % paginationRequest.getPageSize() == 0){
            total = boardCnt / paginationRequest.getPageSize();
        }else {
            total = boardCnt / paginationRequest.getPageSize() + 1;
        }

        paginationResponse.setLists(toDoRepository.search(keyword, paginationRequest));
        paginationResponse.setTotal(total);
        paginationResponse.setAmount(boardCnt);

        return paginationResponse;
    }

    @Override
    public void modify(ToDoBoard toDoBoard) {

        toDoBoard.setUpdatedDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        toDoRepository.modify(toDoBoard);
    }

    @Override
    public void remove(Long id) {

        toDoRepository.remove(id);
    }

    @Override
    public ToDoBoard read(Long id) {

        return toDoRepository.read(id);
    }

    @Override
    public void finishOrReset(Long id) {

        ToDoBoard getToDo = toDoRepository.read(id);

        if (getToDo.getState().equals("미완료")){
            getToDo.setState("완료");
        }else if (getToDo.getState().equals("완료")){
            getToDo.setState("미완료");
        }

        toDoRepository.modify(getToDo);
    }
}
